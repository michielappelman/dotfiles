#!/usr/bin/env bash

# Symlinks to Documents folder
dirs=("Music" "Movies" "Pictures" "sources" "sources/bin")
for dir in "${dirs[@]}"; do
  LINKDIR="$HOME/Documents/$dir"
  DIRNAME="$(echo $dir | awk -F '/' '{print $NF}')"
  HOMEDIR="$HOME/$DIRNAME"

  if [ -L "$HOMEDIR" ] || ! [ -e "$HOMEDIR" ]
  then
    rm -rv "$HOMEDIR"
    ln -sv "$LINKDIR" "$HOMEDIR"
  else
    echo "WARN: $HOMEDIR exists but is not a link"
  fi
done

# General Settings
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"
defaults write NSGlobalDomain AppleSpacesSwitchOnActivate -bool true
defaults write NSGlobalDomain NSDisableAutomaticTermination -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true
defaults write NSGlobalDomain NSNavPanelFileLastListModeForOpenModeKey -int 2
defaults write NSGlobalDomain NSNavPanelFileListModeForOpenMode2 -int 2
defaults write NSGlobalDomain NavPanelFileListModeForOpenMode -int 2

# Window Options
# Ctrl+Cmd+Click on window to drag it
# https://mmazzarolo.com/blog/2022-04-16-drag-window-by-clicking-anywhere-on-macos/
defaults write NSGlobalDomain NSWindowShouldDragOnGesture -bool true

defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

# Keyboard Settings
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false
defaults write NSGlobalDomain AppleKeyboardUIMode -int 2

# Mouse Settings
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2

# Dock Settings
defaults write com.apple.dock autohide -bool true
defaults write com.apple.dock show-recents -bool false
defaults write com.apple.dock tilesize -int 50
defaults write com.apple.dock largesize -int 69
defaults write com.apple.dock expose-animation-duration -float 0.1

# Spaces Settings
defaults write com.apple.dock mru-spaces -bool false
defaults write com.apple.dashboard mcx-disabled -bool true
defaults write com.apple.dock dashboard-in-overlay -bool true

# Hot Corners
defaults write com.apple.dock wvous-tl-corner -int 0
defaults write com.apple.dock wvous-tl-modifier -int 0
defaults write com.apple.dock wvous-tr-corner -int 2 # expose
defaults write com.apple.dock wvous-tr-modifier -int 0
defaults write com.apple.dock wvous-bl-corner -int 0
defaults write com.apple.dock wvous-bl-modifier -int 0
killall Dock

# Finder Settings
defaults write com.apple.finder NewWindowTarget -string "PfHm"
defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf" # search in current folder
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv" # list
defaults write com.apple.finder WarnOnEmptyTrash -bool false
chflags nohidden ~/Library
killall Finder

# Screenshot Settings
defaults write com.apple.screencapture "type" -string "png"
defaults write com.apple.screencapture "location" -string "${HOME}/Documents/Screenshots"
defaults write com.apple.screencapture "disable-shadow" -bool true
killall SystemUIServer

# Application Settings
# Prevent Photos from automatically starting when inserting USB storage
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true
