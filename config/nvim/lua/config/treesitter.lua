-- Treesitter configuration
if vim.fn.has("mac") == 1 then
	require("nvim-treesitter.install").compilers = { "clang" }
else
	require("nvim-treesitter.install").compilers = { "gcc" }
end
require("nvim-treesitter.configs").setup({
	highlight = {
		enable = true, -- false will disable the whole extension
	},
	ensure_installed = {
		"c",
		"go",
		"gomod",
		"bash",
		"comment",
		"dockerfile",
		"graphql",
    "hcl",
		"html",
		"http",
		"javascript",
		"json",
		"json5",
		"jsonc",
		"lua",
		"make",
		"markdown",
		"ninja",
		"perl",
		"python",
		"regex",
		"rust",
		"scss",
    "terraform",
		"toml",
		"typescript",
		"vim",
		"yaml",
		"yang",
	},
	rainbow = { enable = true, extended_mode = true, max_file_lines = 1000 },
	autotag = { enable = true },
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = "gnn",
			node_incremental = "grn",
			scope_incremental = "grc",
			node_decremental = "grm",
		},
	},
	indent = { enable = true },
	textobjects = {
		select = {
			enable = true,
			lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
			keymaps = {
				-- You can use the capture groups defined in textobjects.scm
				["af"] = "@function.outer",
				["if"] = "@function.inner",
				["ac"] = "@class.outer",
				["ic"] = "@class.inner",
			},
		},
		move = {
			enable = true,
			set_jumps = true, -- whether to set jumps in the jumplist
			goto_next_start = { ["]m"] = "@function.outer", ["]]"] = "@class.outer" },
			goto_next_end = { ["]M"] = "@function.outer", ["]["] = "@class.outer" },
			goto_previous_start = {
				["[m"] = "@function.outer",
				["[["] = "@class.outer",
			},
			goto_previous_end = { ["[M"] = "@function.outer", ["[]"] = "@class.outer" },
		},
	},
})
